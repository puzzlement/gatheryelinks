<?php
/*
    Gather Ye Links, a WordPress link plugin
    Copyright (C) 2012 Mary Gardiner

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$GYL_SETTINGPAGE = 'gatherye_linkssettings';
$GYL_OPTIONSGROUP = 'gatheryelinks_options';
$GYL_OPTIONSECTION = 'gatheryelinks_feeds';
$GYL_FEEDS = 'gatheryelinks_feeds';
$GYL_TWITTER_UN = 'gatheryelinks_twitter_un';
$GYL_TWITTER_SEARCH = 'gatheryelinks_twitter_search';

function gatheryelinks_setting_textarea($args) {
    global $GYL_OPTIONSGROUP;
    $option_name = $args['option_name'];
    $rows = $args['rows'];
   $option_value = get_option($option_name);
   echo "<textarea id='$option_name' name='$option_name' cols='80' rows='{$rows}'>{$option_value}</textarea>";
}

function gatheryelinks_setting_textarea_unavailable($args) {
    global $GYL_OPTIONSGROUP;
    $option_name = $args['option_name'];
    $rows = $args['rows'];
   $option_value = get_option($option_name);
}

function gatheryelinks_options_validate($input) {
    return $input;
}

add_action('admin_init', 'gatheryelinks_admin_init');
function gatheryelinks_admin_init(){
    global $GYL_OPTIONSECTION, $GYL_SETTINGPAGE, $GYL_OPTIONSGROUP;
    global $GYL_FEEDS, $GYL_TWITTER_UN, $GYL_TWITTER_SEARCH;

    add_settings_section($GYL_OPTIONSECTION, 'Feed Settings',
'gatheryelinks_sectioSEARCHn_text', $GYL_SETTINGPAGE);
    register_setting($GYL_OPTIONSGROUP, $GYL_FEEDS, 'gatheryelinks_options_validate' );
    add_settings_field($GYL_FEEDS,
        '<b>Delicious/Pinboard/etc feeds</b> one per line eg<br/><span
style="font-family:fixed-width;">http://feeds.delicious.com/v2/rss/tag/TAGNAME</span><br/><span
style="font-family:fixed-width;">http://feeds.delicious.com/v2/rss/USERNAME</span><br/><span
style="font-family:fixed-width;">http://feeds.pinboard.in/rss/t:TAGNAME</span><br/><span
style="font-family:fixed-width;">http://feeds.pinboard.in/rss/u:USERNAME</span>',
        'gatheryelinks_setting_textarea',
        $GYL_SETTINGPAGE, $GYL_OPTIONSECTION,
        $args = array("option_name" => $GYL_FEEDS, 'rows' => 7));

    register_setting($GYL_OPTIONSGROUP, $GYL_TWITTER_UN, 'gatheryelinks_options_validate' );
    add_settings_field($GYL_TWITTER_UN,
        'Coming soon: <b>Twitter usernames</b> one per line',
        'gatheryelinks_setting_textarea_unavailable',
        $GYL_SETTINGPAGE, $GYL_OPTIONSECTION,
        $args = array('option_name' => $GYL_TWITTER_UN, 'rows' => 5));

    register_setting($GYL_OPTIONSGROUP, $GYL_TWITTER_SEARCH, 'gatheryelinks_options_validate' );
    add_settings_field($GYL_TWITTER_SEARCH,
        'Coming soon: <b>Twitter search terms</b> one per line eg<br/>blogging<br/>#blogging',
        'gatheryelinks_setting_textarea_unavailable',
        $GYL_SETTINGPAGE, $GYL_OPTIONSECTION,
        $args = array('option_name' => $GYL_TWITTER_SEARCH, 'rows' => 5));
}


add_action( 'admin_menu', 'gatheryelinks_menu' );

function gatheryelinks_menu() {
    add_options_page( 'Gather Ye Links Options', 'Gather Ye Links', 'manage_options',
'gather-ye-links', 'gatheryelinks_options' );
}

function gatheryelinks_options() {
    global $GYL_SETTINGPAGE;
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this
page.' ) );
    }
    echo '<div class="wrap">';
    echo 'Gather Ye Links plugin options';
    echo ' <form action="options.php" method="post">';
    settings_fields('gatheryelinks_options'); 
    do_settings_sections($GYL_SETTINGPAGE);
    echo  '<input type="submit" name="submit" value="Save Options" id="bnc-button" class="button-primary" />';
    echo '</form></div>';
}


function gatheryelinks_section_text() {
    echo '<p>Settings for Gather Ye Links feeds.</p>';
}

