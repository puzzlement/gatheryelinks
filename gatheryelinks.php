<?php
/*
Plugin Name: Gather Ye Links
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 0.0.1
Author: Mary Gardiner
Author URI: http://mary.gardiner.id.au/
License: GPL3
*/

/*
    Gather Ye Links, a WordPress link plugin
    Copyright (C) 2012 Mary Gardiner

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// [gatherlinks days="n"]

require('magpierss/rss_fetch.inc');

require_once(plugin_dir_path( __FILE__ ) . 'settings.php');

class Link {

    public $link;
    public $title;
    public $description;

    public function Link($link, $title, $description) {
        $this->link = $link;
        $this->title = $title;
        $this->description = $description;
    }

    public function displayAsLi() {
        $value = "<li><a href=\"{$this->link}\">{$this->title}</a>";
        if (strlen($this->description) > 0) {
            $value = $value . ": " . $this->description;
        }
        return $value . "</li>";
    }
}

function gyl_get_individ_option($option) {
    return preg_split('/[\r\n]+/', get_option($option));
}

function gyl_get_timestr($item) {
    if (strlen($item['pubdate']) > 0) {
        $timestr = $item['pubdate'];
    } elseif (strlen($item['dc']['date']) > 0) {
        $timestr = $item['dc']['date'];
    }
    return $timestr;
}

function gyl_check_rss_age($item, $days) {
    $maxdiff = 24 * 60 * 60 * $days;
    $timestr = gyl_get_timestr($item);
    if ((time() - strtotime($timestr)) < $maxdiff) {
        return True;
    } else {
        return False;
    }
}

function gyl_get_rss_links($days)  {
    global $GYL_FEEDS;
    $links = array();
    $feeds = gyl_get_individ_option($GYL_FEEDS);
    $stripped_feeds = array();
    foreach ($feeds as $feed) {
        $url = trim($feed);
        $rss = fetch_rss($url);
        if ($rss != False) {
            foreach ($rss->items as $item) {
                if (gyl_check_rss_age($item, $days)) {
                    $links[$item['link']] = new Link($item['link'], $item['title'], $item['description']);
#                    $links[$item['link']] = new Link($item['link'],
#$item['title'], gyl_get_timestr($item) . print_r($item, $return
#= True));
                }
            }
        }
    }
    return $links;
}

function gyl_get_replacement($matches) {
    $days = $matches[1];
    $replacement = '';
    $links = gyl_get_rss_links($days);
    foreach ($links as $link) {
#        $replacement = $replacement . print_r($link, $return = True);
        $replacement = $replacement . $link->displayAsLi() . "\r\n";
    }
    return $replacement;
}

function gyl_replacer($content) {
    $re = '/\[gatherlinks +days="?(\d*)"?\]/';
    $replacement = preg_replace_callback($re , gyl_get_replacement, $content, 1);
    return $replacement;
}
 
add_filter('the_content', 'gyl_replacer');
add_filter('content_edit_pre', 'gyl_replacer');

?>
